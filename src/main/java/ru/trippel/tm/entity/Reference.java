package ru.trippel.tm.entity;

import java.util.ArrayList;

public class Reference {
    public final static String HELP_MESSAGE = "HELP";
    private final static ArrayList<String> referenceList = new ArrayList<String>();

    static {
        referenceList.add("project_create - create new project");
        referenceList.add("project_view - show all project");
        referenceList.add("project_edit - edit project");
        referenceList.add("project_delete - delete project");

        referenceList.add("task_create - create new project");
        referenceList.add("task_view - show all project");
        referenceList.add("task_edit - edit project");
        referenceList.add("task_delete - delete project");
    }

    public static ArrayList<String> getReferenceList() {
        return referenceList;
    }
}
