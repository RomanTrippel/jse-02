package ru.trippel.tm.entity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task {
    private static ArrayList<Task> taskList = new ArrayList<Task>();
    private String name;

    public static ArrayList<Task> getTaskList() {
        return taskList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Task(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
