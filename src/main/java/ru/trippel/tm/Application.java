package ru.trippel.tm;

import ru.trippel.tm.controller.RequestVerification;
import ru.trippel.tm.util.Greeting;
import ru.trippel.tm.util.ReaderKeyboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {
    public static void main(String[] args) throws IOException {
        RequestVerification check = new RequestVerification();
        Greeting greeting = new Greeting();
        ReaderKeyboard readerKeyboard = new ReaderKeyboard();

        //приветсвтие
        greeting.greetingPrint();

        //считали с клавиатуры и отправили запрос в обработку.
        while (true) {
            check.add(readerKeyboard.read());
        }

    }
}
