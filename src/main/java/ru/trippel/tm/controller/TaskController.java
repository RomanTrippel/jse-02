package ru.trippel.tm.controller;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.util.ReaderKeyboard;

import java.io.IOException;

public class TaskController {
    ReaderKeyboard readerKeyboard = new ReaderKeyboard();
    Task task;

    public void create() throws IOException {
        System.out.println("Enter task name:");
        Task.getTaskList().add(new Task(readerKeyboard.read()));
        System.out.println("Task added");
    }

    public void view() {
        System.out.println("Task list:");
        for (int i = 0; i < Task.getTaskList().size(); i++) {
            System.out.println(i+1 + ". " + Task.getTaskList().get(i));
        }
    }

    public void edit() throws IOException {
        int number;
        String newName;
        view();
        System.out.println("Select a task to edit:");
        number = Integer.parseInt(readerKeyboard.read());
        System.out.println("Enter a new name:");
        newName = readerKeyboard.read();
        Task.getTaskList().get(number-1).setName(newName);
        System.out.println("Task name changed");
    }

    public void delete() throws IOException {
        int number;
        view();
        System.out.println("Select a project to delete:");
        number = Integer.parseInt(readerKeyboard.read());
        Task.getTaskList().remove(number-1);
        System.out.println("Task deleted");

    }
}
