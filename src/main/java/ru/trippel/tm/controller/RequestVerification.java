package ru.trippel.tm.controller;

import ru.trippel.tm.entity.Reference;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.util.ReferencePrint;

import java.io.IOException;

public class RequestVerification {
    ProjectController projectController = new ProjectController();
    TaskController taskController = new TaskController();
    ReferencePrint referencePrint = new ReferencePrint();

    //проверяет запрос на соответствие, если не соответствует, просьба на ввод заново.
    public void add(String request) throws IOException {
        //проверка на запрос справки "HELP"
        if (Reference.HELP_MESSAGE.equals(request)) {
            System.out.println("COMMAND LIST:");
           referencePrint.referencePrintList();
        }
        //проверка на команды Проекта
        else if ("project_create".equals(request)) {
            projectController.create();
        }
        else if ("project_view".equals(request)) {
            projectController.view();
        }
        else if ("project_edit".equals(request)) {
            projectController.edit();
        }
        else if ("project_delete".equals(request)) {
            projectController.delete();
        }
        //проверка на команды Задач
        else if ("task_create".equals(request)) {
            taskController.create();
        }
        else if ("task_view".equals(request)) {
            taskController.view();
        }
        else if ("task_edit".equals(request)) {
            taskController.edit();
        }
        else if ("task_delete".equals(request)) {
            taskController.delete();
        }
        //в случае не корректного запроса
        else System.out.println("You entered an incorrect command, try again.");

    }
}
