## java-core-online

### must have (SOFTWARE):
```
* Java 8
* JRE
* Maven 4.0.0
```

## Commands to build:
```
mvn clean
mvn install
```

## Command to run:
```
java -jar java-core-online-2.0-SNAPSHOT.jar
```
### Developer:
```
Roman Trippel

rtrippel84@gmail.com
```